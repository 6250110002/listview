import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // const MyApp({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];
  final images = [
    NetworkImage('https://media.discordapp.net/attachments/769447787237998595/934010740036218890/unknown.png?width=497&height=497'),
    NetworkImage(
        'https://media.discordapp.net/attachments/769447787237998595/934010930382127124/unknown.png'),
    NetworkImage(
        'https://media.discordapp.net/attachments/769447787237998595/934011198989557760/unknown.png?width=497&height=497'),
    NetworkImage(
        'https://media.discordapp.net/attachments/769447787237998595/934015549179060234/unknown.png?width=497&height=497'),
    NetworkImage(
        'https://media.discordapp.net/attachments/769447787237998595/934011789484650516/unknown.png?width=497&height=497'),
    NetworkImage('https://media.discordapp.net/attachments/769447787237998595/934012268499320843/unknown.png?width=497&height=497'),
    NetworkImage(
        'https://media.discordapp.net/attachments/769447787237998595/934012449299005500/unknown.png'),
    NetworkImage(
        'https://media.discordapp.net/attachments/769447787237998595/934014673269981184/unknown.png'),
    NetworkImage(
        'https://media.discordapp.net/attachments/769447787237998595/934014883647852544/unknown.png'),
  ];

  // final icons = [
  //   Icons.directions_bike,
  //   Icons.directions_boat,
  //   Icons.directions_bus,
  //   Icons.directions_car,
  //   Icons.directions_railway,
  //   Icons.directions_run,
  //   Icons.directions_subway,
  //   Icons.directions_transit,
  //   Icons.directions_walk
  // ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic ListView',
      theme: ThemeData(
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text('List View'),
          ),
          body: ListView.builder(
            itemCount: titles.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: images[index],
                    ),
                    title: Text(
                      '${titles[index]}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text(
                      'There are many passengers in serveral vehicles',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                    trailing: Icon(
                      Icons.notifications_none,
                      size: 25,
                    ),
                    onTap: () {
                      AlertDialog alert = AlertDialog(
                        title: Text(
                            'Welcome'), // To display the title it is optional
                        content: Text(
                            'This is a ${titles[index]}'), // Message which will be pop up on the screen
                        // Action widget which will provide the user to acknowledge the choice
                        actions: [
                          ElevatedButton(
                            // FlatButton widget is used to make a text to work like a button

                            onPressed: () {
                              Navigator.of(context).pop();
                            }, // function used to perform after pressing the button
                            child: Text('CANCEL'),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ACCEPT'),
                          ),
                        ],
                      );
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return alert;
                        },
                      );
                    },
                  ),
                  Divider(
                    thickness: 0.8,
                  ),
                ],
              );
            },
          )),
    );
  }
}
